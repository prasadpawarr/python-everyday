import calendar

def findDay(date):
	day, month, year = [int(i) for i in date.split(" ")]
	dayNumber = calendar.weekday(year, month, day)
	days = ["Monday", "Tuesday", 
			"Wednesday", "Thursday", "Friday", "Sat", "Sunday"]
			
	print(date + " falls on " + days[dayNumber])

def main():
	date = input("Enter a date : (dd mm yyyy) ")
	findDay(date)

if __name__ == "__main__":
	main()
