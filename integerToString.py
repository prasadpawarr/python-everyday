"""
string processing
given an integer,
convert it into string
"""

def int_to_string(i):
  str = []

  if i >= 0:
    is_pos = True
  else:
    is_pos = False

  if(is_pos == False):  
    i = i*(-1)

  while(i):
    str.append(chr((ord('0')) + i % 10))
    #ord - gives unicode(int) for the ascii(char)
    #chr - gives ascii for the unicode int.
    i = i//10

  if(is_pos == False):
    str.append('-')
  return str[::-1]


def main():
  inp = int(input("Enter an integer: "))
  strng = int_to_string(inp)
  op = ''.join(strng)
  print(op)
main()